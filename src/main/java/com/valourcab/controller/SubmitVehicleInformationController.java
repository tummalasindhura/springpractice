package com.valourcab.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
/**
 * 
 * @author sindhura
 *
 */
@Controller
public class SubmitVehicleInformationController {
	
	
	@RequestMapping(value = "/submitdetails", method = RequestMethod.GET)
	public ModelAndView getSubmissionForm() {
		ModelAndView model = new ModelAndView("submissionform");
		return model;
	}

	@ModelAttribute
	public void addingCommonObject(Model model1) {
		model1.addAttribute("headerMessage", "Welcome To SigmaCabs");
	}

	@RequestMapping(value = "/vehiclesubmitForm", method = RequestMethod.POST)
	public ModelAndView submitForm(@ModelAttribute("vehicleBean") VehicleBean vehicleBean) {
		ModelAndView model1 = new ModelAndView("vehicledetailssubmissionformsucess");
		return model1;

	}

}
