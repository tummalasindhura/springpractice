package com.valourcab.controller;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class User {


    public long id;

   public String name;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm a z")
    public Date dob=new Date();

    public Integer age=29;
    
    
    public User(String name, String id) {
    	this.id = (new Date()).getTime();
    	this.name = name;	
    }


    public User() {
		
	}


	public User(String name) {
		// TODO Auto-generated constructor stub
	}


	public long getId() {
        return id;
    }

    public void setId( long id ) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob( Date dob ) {
        this.dob = dob;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge( Integer age ) {
        this.age = age;
    }

    
   

}
