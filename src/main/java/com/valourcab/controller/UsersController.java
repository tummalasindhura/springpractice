package com.valourcab.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping( value = "/users")
public class UsersController {
	
	@RequestMapping(value = "/persons", method = RequestMethod.GET)
    public String user(Model model) {
    	List<User> users = new ArrayList<User>();
    	User one = new User("sindhu", "1");
		User two = new User("keerthi", "2");
		User three = new User("jagan ji", "3");
		users.add(one);
		users.add(two);
		users.add(three);
		model.addAttribute("users", users);
		return "personview"; 	
    }
	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public ModelAndView helloWorld() {
		ModelAndView model = new ModelAndView("HelloPage");
		model.addObject("msg", "Hello Sindhu");
		return model;
		
	}
}
