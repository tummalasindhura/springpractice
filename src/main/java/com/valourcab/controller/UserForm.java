package com.valourcab.controller;

import java.util.List;

public class UserForm {

	public List<User> users;

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
}
