package com.valourcab.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping( value = "/add")
public class WelcomeController {
	
	
	private static List<User> users = new ArrayList<User>();
	static {
		users.add(new User("Sindhu", "1"));
		users.add(new User("Keerthi", "2"));
		users.add(new User("JaganJi", "3"));
		
		
	}
	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public ModelAndView get() {
		UserForm userForm = new UserForm();
		userForm.setUsers(users);
		return new ModelAndView("UserForm" , "userForm", userForm);
	}
	
	
	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public ModelAndView helloWorld() {
		ModelAndView model = new ModelAndView("HelloPage");
		model.addObject("msg", "helloworld");
		return model;
		
	}
	
	/*@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("contactForm") ContactForm contactForm) {
		System.out.println(contactForm);
		System.out.println(contactForm.getContacts());
		List<Contact> contacts = contactForm.getContacts();
		
		if(null != contacts && contacts.size() > 0) {
			ContactController.contacts = contacts;
			for (Contact contact : contacts) {
				System.out.printf("%s \t %s \n", contact.getFirstname(), contact.getLastname());
			}
		}
		
		return new ModelAndView("show_contact", "contactForm", contactForm);
	}*/
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("userForm")  UserForm userForm) {
		
		userForm.setUsers(users);
		System.out.println(userForm);
		System.out.println(userForm.getUsers());
		List<User> users = userForm.getUsers();
		if(null != users && users.size() > 0) {
			WelcomeController.users = users;
			for (User user : users) {
				System.out.printf("%s \t %s \n",user.getName(), user.getId());
			}
		}
		return new ModelAndView("ShowUser", "userForm", userForm);
		
		
	}
	

}
