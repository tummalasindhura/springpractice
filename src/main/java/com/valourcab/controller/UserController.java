package com.valourcab.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping( value = "/user", produces = MediaType.APPLICATION_JSON_VALUE )
public class UserController {

    @RequestMapping( value = "/{username}", method = RequestMethod.GET )
    public User getUser( @PathVariable( "username" ) String username ) {

        final User user = new User();
        user.setName( username );
        return user;
    }
    
    @RequestMapping( value = "/", method = RequestMethod.GET) 
    public List<User> getUsers() {
    	List<User> users = new ArrayList<User>();
    	
		User user = new User();
		User one = new User("sindhu", "1");
		User two = new User("keerthi", "2");
		User three = new User("shiv", "3");

		users.add(one);
		users.add(two);
		users.add(three);
		return users;
    	
    }
    
    
   
    
    @RequestMapping(value = "/persons", method = RequestMethod.GET)
    public String user(Model model) {
    	List<User> users = new ArrayList<User>();
    	User one = new User("sindhu", "1");
		User two = new User("keerthi", "2");
		User three = new User("g", "3");
		users.add(one);
		users.add(two);
		users.add(three);
		model.addAttribute("persons", users);
		return "personview";
    	
    	
    }
    
    
    
   
	
    
}
