package com.valourcab;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner.Mode;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * @author ThirupathiReddy V
 */
@SpringBootApplication(exclude=SecurityAutoConfiguration.class)
public class MainApplication extends SpringBootServletInitializer {

    /** Reference to logger */
    private static final Logger LOG = LoggerFactory.getLogger( MainApplication.class );

    public static void main( String[] args ) {

        LOG.info( "Spring Boot Application " );

        final SpringApplication application = new SpringApplication( MainApplication.class );
        final Properties properties = new Properties();
        // properties.put("server.servletPath", "/api/*");// dispatch-servlet path can be set here
        application.setBannerMode( Mode.CONSOLE );
        application.setDefaultProperties( properties );
        application.run();

    }

}
